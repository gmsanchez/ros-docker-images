Docker ROS Images
====

> ⚠️ Use this images only for development ⚠️ 

# Library

**Namming**:

    <PACKAGE>/<PACKAGE_VERSION>-<OS_VERSION>-<VARIANT>

## ROS

Images for ROS1 with development packages and *display forwarding* for GUI.

> Check [REP-142](https://www.ros.org/reps/rep-0142.html) for metapackages content

### Available tags (under `ros\`)

- `noetic-focal-base`:
  - FROM: `ubuntu:focal`
  - Packages: rosbase, rosdep, catkin-tools (and others)
- `noetic-focal-desktop`:
  - FROM: noetic-focal-base
  - Packages: rosrobot (metapackage) without robot_model, rosviz (metapackage) (and others)
- `noetic-focal-gui`:
  - FROM: noetic-focal-desktop
  - Packages: mesa-utils, libgl1-mesa-dri, x-window-system (and others)
- `noetic-focal-gazebo`:
  - FROM: noetic-focal-gui
  - Packages: gazebo, plotjuggler

### Run example

```console
$ docker run -it \
-e DISPLAY=$DISPLAY
--mount type=bind,source=/tmp/.X11-unix,target=/tmp/.X11-unix,readonly=false \
--device=/dev/dri:/dev/dri \
registry.gitlab.com/acapovilla/ros-docker-images/ros/noetic-focal-gazebo:latest
```

## ROS2

Images for ROS2 with development packages and *display forwarding* for GUI.

> Check [REP-2001](https://ros.org/reps/rep-2001.html) for variants content

### Available tags (under `ros2\`)

- `humble-jammy-base`:
  - FROM: `ubuntu:jammy`
  - Packages: ROS Base variant with rosdep, ament and colcon (and others)
- `humble-jammy-desktop`:
  - FROM: humble-jammy-base
  - Packages: Desktop variant without tutorials, demos and examples packages
- `humble-jammy-gui`:
  - FROM: humble-jammy-desktop
  - Packages: mesa-utils, libgl1-mesa-dri, x-window-system (and others)
- `humble-jammy-desktop-full`:
  - FROM: humble-jammy-gui
  - Packages: Perception and Simulation variants with plotjuggler

### Run example

```console
$ docker run -it \
-e DISPLAY=$DISPLAY
--mount type=bind,source=/tmp/.X11-unix,target=/tmp/.X11-unix,readonly=false \
--device=/dev/dri:/dev/dri \
registry.gitlab.com/acapovilla/ros-docker-images/ros2/humble-jammy-desktop-full:latest
```

----

# Extras

## Based on

- https://github.com/osrf/docker_images

## References / Tutorials

- http://wiki.ros.org/docker/Tutorials/GUI
- http://wiki.ros.org/docker/Tutorials/Hardware%20Acceleration
- https://tuw-cpsg.github.io/tutorials/docker-ros/
- https://roboticseabass.com/2021/04/21/docker-and-ros/
- http://fabiorehm.com/blog/2014/09/11/running-gui-apps-with-docker/
